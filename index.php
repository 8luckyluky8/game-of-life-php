<?php

require __DIR__ . '/vendor/autoload.php';

$game = new \GameOfLife\GameFacade();
$game->run();