<?php

namespace GameOfLife;


class CellRowCollection implements \IteratorAggregate, \ArrayAccess, \Countable
{
    /** @var array */
    private $rows = [];

    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->rows);
    }

    public function offsetExists($offset): bool
    {
        return isset($this->rows[$offset]);
    }

    public function offsetGet($offset): ?CellCollection
    {
        return $this->rows[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        if (is_scalar($offset))
        {
            $this->rows[$offset] = $value;
        }
        else
        {
            $this->rows[] = $value;
        }
    }

    public function offsetUnset($offset): void
    {
        unset($this->rows[$offset]);
    }

    public function count(): int
    {
        return count($this->rows);
    }
}