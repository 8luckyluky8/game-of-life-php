<?php

namespace GameOfLife;


class GameFacade
{
    /** @const string */
    const BOARD_SIZE_DELIMITER = 'x';

    /** @var int */
    private $boardWidth = 0;
    /** @var int */
    private $boardHeight = 0;
    /** @var int */
    private $aliveCellsCount = 0;

    /** @var Game */
    private $game;

    private function write(string $message, bool $newLine = true): void
    {
        fwrite(STDOUT, $message . ($newLine ? "\n" : ""));
    }

    private function read(): string
    {
        return fgets(STDIN);
    }

    /**
     * Render start of the game and inputs.
     */
    private function renderStart(): void
    {
        $this->write("Game of Life");

        // Load size of board
        $validSize = false;
        while (!$validSize)
        {
            $this->write(sprintf("Please select board size (default: %d%s%d): ", Board::DEFAULT_WIDTH, self::BOARD_SIZE_DELIMITER, Board::DEFAULT_HEIGHT), false);
            $size = trim($this->read());

            // If the input is empty, use default values
            if (strlen($size) === 0)
            {
                $this->boardWidth = Board::DEFAULT_WIDTH;
                $this->boardHeight = Board::DEFAULT_HEIGHT;

                $validSize = true;
            }
            else
            {
                $sizes = explode(self::BOARD_SIZE_DELIMITER, $size);
                if (count($sizes) !== 2)
                {
                    $this->write("Invalid size. Please enter two digits separated with " . self::BOARD_SIZE_DELIMITER);
                }
                else
                {
                    $validNumbers = true;

                    foreach ($sizes as $index => $inputSize)
                    {
                        if (!is_numeric($inputSize))
                        {
                            $this->write("Invalid size. Please enter two digits separated with " . self::BOARD_SIZE_DELIMITER);

                            $validNumbers = false;
                        }
                        else
                        {
                            if ($index === 0)
                            {
                                $this->boardWidth = (int)$inputSize;
                            }
                            else
                            {
                                $this->boardHeight = (int)$inputSize;
                            }
                        }
                    }

                    $validSize = $validNumbers;
                }
            }
        }

        // Load number of alive cells
        $validAliveCellsCount = false;
        $maxCellCount = $this->boardWidth * $this->boardHeight;
        while (!$validAliveCellsCount)
        {
            $this->write(sprintf("Please select initial alive cells count (default: %d): ", Board::DEFAULT_ACTIVE_CELLS_COUNT), false);

            $aliveCellsCount = trim($this->read());
            // If the input is empty, use default value
            if (strlen($aliveCellsCount) === 0)
            {
                $this->aliveCellsCount = Board::DEFAULT_ACTIVE_CELLS_COUNT;

                $validAliveCellsCount = true;
            }
            else
            {
                if (!is_numeric($aliveCellsCount))
                {
                    $this->write("Invalid number. Please enter a digit.");
                }
                else
                {
                    if ($aliveCellsCount > $maxCellCount)
                    {
                        $this->write("Max. number of alive cells is {$maxCellCount}.");
                    }
                    else
                    {
                        $this->aliveCellsCount = (int)$aliveCellsCount;

                        $validAliveCellsCount = true;
                    }
                }
            }
        }
    }

    /**
     * Get alive cell string representation
     * @return string
     */
    private function getAliveCellString(): string
    {
        return "|X|";
    }

    /**
     * Get dead cell string representation
     * @return string
     */
    private function getDeadCellString(): string
    {
        return "|_|";
    }

    /**
     * Render current board state to console.
     */
    private function renderBoard(): void
    {
        $output = "";

        $board = $this->game->getBoard();
        $boardWidth = $board->getWidth();

        for ($i = 0; $i < $boardWidth; $i++)
        {
            $output .= " _ ";
        }

        $output .= "\n";

        /** @var CellCollection $row */
        foreach ($board as $row)
        {
            /** @var Cell $cell */
            foreach ($row as $cell)
            {
                $output .= $cell->isAlive() ? $this->getAliveCellString() : $this->getDeadCellString();
            }

            $output .= "\n";
        }

        $this->write($output);
    }

    /**
     * Render current step (board and prompt for key press).
     */
    private function renderStep(): void
    {
        $this->renderBoard();

        $this->write("Press key to continue: ", false);
        $this->read();
    }

    /**
     * Run game
     */
    public function run(): void
    {
        $this->renderStart();

        $this->game = new Game($this->boardWidth, $this->boardHeight, $this->aliveCellsCount);

        while (true)
        {
            $this->renderStep();
            $this->game->nextStep();
        }
    }
}