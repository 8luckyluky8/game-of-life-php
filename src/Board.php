<?php

namespace GameOfLife;

class Board implements \IteratorAggregate, \ArrayAccess, \Countable
{
    const DEFAULT_WIDTH = 10;
    const DEFAULT_HEIGHT = 10;
    const DEFAULT_ACTIVE_CELLS_COUNT = 10;

    /** @var CellRowCollection */
    private $rows;

    public function __construct(int $width = self::DEFAULT_HEIGHT, int $height = self::DEFAULT_HEIGHT, int $initialAliveCellsCount = 0)
    {
        $this->initBoard($width, $height, $initialAliveCellsCount);
    }

    public function getIterator(): \ArrayIterator
    {
        return $this->rows->getIterator();
    }

    public function offsetExists($offset): bool
    {
        return $this->rows->offsetExists($offset);
    }

    public function offsetGet($offset): ?CellCollection
    {
        return $this->rows->offsetGet($offset);
    }

    public function offsetSet($offset, $value): void
    {
        $this->rows->offsetSet($offset, $value);
    }

    public function offsetUnset($offset): void
    {
        $this->rows->offsetUnset($offset);
    }

    public function count(): int
    {
        return $this->rows->count();
    }

    /**
     * Generate random coords for alive cells
     * @param int $width
     * @param int $height
     * @param int $count
     * @return CoordCollection
     */
    private function generateRandomAliveCells(int $width, int $height, int $count): CoordCollection
    {
        $boardSize = $width * $height;
        if ($count > $boardSize)
        {
            $count = $boardSize;
        }

        $aliveCellsCoords = new CoordCollection();

        for ($i = 0; $i < $count; $i++)
        {
            do {
                $x = rand(0, $width - 1);
                $y = rand(0, $height - 1);
            } while ($aliveCellsCoords->isCoordSet($x, $y));

            $aliveCellsCoords[] = new Coord($x, $y);
        }

        return $aliveCellsCoords;
    }

    /**
     * Initialize board with given size and alive cells count
     * @param int $width
     * @param int $height
     * @param int $initialAliveCellsCount
     */
    private function initBoard(int $width, int $height, int $initialAliveCellsCount): void
    {
        $aliveCells = null;
        if ($initialAliveCellsCount > 0)
        {
            $aliveCells = $this->generateRandomAliveCells($width, $height, $initialAliveCellsCount);
        }

        $this->rows = new CellRowCollection();

        for ($y = 0; $y < $height; $y++)
        {
            $this->rows[$y] = new CellCollection();

            for ($x = 0; $x < $width; $x++)
            {
                $cell = new Cell(new Coord($x, $y));

                if ($aliveCells && $aliveCells->isCoordSet($x, $y))
                {
                    $cell->setAlive();
                }

                $this->rows[$y][$x] = $cell;
            }
        }
    }

    /**
     * Initialize empty board with given size
     * @param int $width
     * @param int $height
     */
    private function initEmptyBoard(int $width, int $height): void
    {
        $this->initBoard($width, $height, 0);
    }

    public function getAliveCellsCount(): int
    {
        $aliveCellsCount = 0;

        foreach ($this->rows as $row)
        {
            /** @var Cell $cell */
            foreach ($row as $cell)
            {
                if ($cell->isAlive())
                {
                    $aliveCellsCount++;
                }
            }
        }

        return $aliveCellsCount;
    }

    public function getHeight(): int
    {
        return count($this->rows);
    }

    public function getWidth(): int
    {
        if (!$this->getHeight())
        {
            return 0;
        }

        return count($this->rows[0]);
    }

    /**
     * Get number of alive neighbours of cell on given coords
     * @param int $x
     * @param int $y
     * @return int
     */
    public function getCellAliveNeighboursCount(int $x, int $y): int
    {
        if (!isset($this->rows[$y][$x]))
        {
            return 0;
        }

        $aliveNeighboursCount = 0;
        $boardWidth = $this->getWidth();
        $boardHeight = $this->getHeight();

        for ($i = $y - 1; $i <= $y + 1; $i++)
        {
            if ($i < 0 || $i >= $boardHeight)
            {
                continue;
            }

            for ($j = $x - 1; $j <= $x + 1; $j++)
            {
                if ($j < 0 || $j >= $boardWidth)
                {
                    continue;
                }
                if ($i === $y && $j === $x)
                {
                    continue;
                }

                /** @var Cell $cell */
                $cell = $this->rows[$i][$j];
                if ($cell->isAlive())
                {
                    $aliveNeighboursCount++;
                }
            }
        }

        return $aliveNeighboursCount;
    }

    public function getCell(int $x, int $y): ?Cell
    {
        if (!isset($this->rows[$y][$x]))
        {
            return null;
        }

        return $this->rows[$y][$x];
    }
}