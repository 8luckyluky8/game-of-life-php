<?php

namespace GameOfLife;

class CellCollection implements \IteratorAggregate, \ArrayAccess, \Countable
{
    /** @var array */
    private $cells = [];

    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->cells);
    }

    public function offsetExists($offset): bool
    {
        return isset($this->cells[$offset]);
    }

    public function offsetGet($offset): ?Cell
    {
        return $this->cells[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        if (is_scalar($offset))
        {
            $this->cells[$offset] = $value;
        }
        else
        {
            $this->cells[] = $value;
        }
    }

    public function offsetUnset($offset): void
    {
        unset($this->cells[$offset]);
    }

    public function count(): int
    {
        return count($this->cells);
    }
}