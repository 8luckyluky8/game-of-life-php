<?php

namespace GameOfLife;


class Game
{
    /** @const int Number of alive neighbours required to set cell state to alive */
    const ALIVE_STATE_COUNT = 3;
    /** @const int Number of alive neighbours required to keep cells current state */
    const SAME_STATE_COUNT = 2;

    /** @var Board */
    private $board;

    public function __construct(int $boardWidth = Board::DEFAULT_WIDTH, int $boardHeight = Board::DEFAULT_HEIGHT, int $initialAliveCellsCount = 0)
    {
        $this->board = new Board($boardWidth, $boardHeight, $initialAliveCellsCount);
    }

    /**
     * Returns new cell state according to game rules.
     * (Cell needs 3 alive neighbours to stay/become alive and 2 alive neighbours to keep the current state.
     * Everything other than that means the cell is dead.)
     * @param int $x
     * @param int $y
     * @return bool
     */
    public function getCellNewState(int $x, int $y): bool
    {
        $cell = $this->board->getCell($x, $y);
        if (!$cell)
        {
            return Cell::DEAD_STATE;
        }

        $aliveNeighboursCount = $this->board->getCellAliveNeighboursCount($x, $y);
        if ($aliveNeighboursCount === self::ALIVE_STATE_COUNT)
        {
            return Cell::ALIVE_STATE;
        }

        if ($aliveNeighboursCount === self::SAME_STATE_COUNT && $cell->isAlive())
        {
            return Cell::ALIVE_STATE;
        }

        return Cell::DEAD_STATE;
    }

    /**
     * Calculates new board for the next step.
     * @return Board
     */
    public function getNextStepBoard()
    {
        $newBoard = new Board($this->board->getWidth(), $this->board->getHeight());

        /** @var CellCollection $row */
        foreach ($this->board as $row)
        {
            /** @var Cell $cell */
            foreach ($row as $cell)
            {
                $x = $cell->getCoord()->getX();
                $y = $cell->getCoord()->getY();

                $newCell = new Cell(new Coord($x, $y));
                $newCellState = $this->getCellNewState($x, $y);
                if ($newCellState == Cell::ALIVE_STATE)
                {
                    $newCell->setAlive();
                }

                $newBoard[$y][$x] = $newCell;
            }
        }

        return $newBoard;
    }

    /**
     * Moves board to the next step.
     */
    public function nextStep()
    {
        $this->board = $this->getNextStepBoard();
    }

    /**
     * @return Board
     */
    public function getBoard(): Board
    {
        return $this->board;
    }
}