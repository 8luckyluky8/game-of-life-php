<?php

namespace GameOfLife;


class CoordCollection implements \IteratorAggregate, \ArrayAccess, \Countable
{
    /** @var array */
    private $coords = [];

    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->coords);
    }

    public function offsetExists($offset): bool
    {
        return isset($this->coords[$offset]);
    }

    public function offsetGet($offset): ?Coord
    {
        return $this->coords[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        if (is_scalar($offset))
        {
            $this->coords[$offset] = $value;
        }
        else
        {
            $this->coords[] = $value;
        }
    }

    public function offsetUnset($offset): void
    {
        unset($this->coords[$offset]);
    }

    public function count(): int
    {
        return count($this->coords);
    }

    public function isCoordSet($x, $y): bool
    {
        /** @var Coord $coord */
        foreach ($this->coords as $coord)
        {
            if ($x === $coord->getX() && $y === $coord->getY())
            {
                return true;
            }
        }

        return false;
    }
}