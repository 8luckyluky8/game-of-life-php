<?php

namespace GameOfLife;

class Cell
{
    const ALIVE_STATE = true;
    const DEAD_STATE = false;

    /** @var bool */
    private $alive;
    /**
     * @var Coord
     */
    private $coord;

    public function __construct(Coord $coord, bool $alive = self::DEAD_STATE)
    {
        $this->alive = $alive;
        $this->coord = $coord;
    }

    public function setAlive(): void
    {
        $this->alive = self::ALIVE_STATE;
    }

    public function setDead(): void
    {
        $this->alive = self::DEAD_STATE;
    }

    public function isAlive(): bool
    {
        return $this->alive === self::ALIVE_STATE;
    }

    /**
     * @return Coord
     */
    public function getCoord(): Coord
    {
        return $this->coord;
    }
}