<?php


class CellTest extends \PHPUnit\Framework\TestCase
{
    public function testSetAliveCell()
    {
        $cell = new \GameOfLife\Cell(new \GameOfLife\Coord(0, 0));
        $this->assertEquals(\GameOfLife\Cell::DEAD_STATE, $cell->isAlive());

        $cell->setAlive();
        $this->assertEquals(\GameOfLife\Cell::ALIVE_STATE, $cell->isAlive());
    }

    public function testSetDeadCell()
    {
        $cell = new \GameOfLife\Cell(new \GameOfLife\Coord(0, 0));

        $cell->setAlive();
        $cell->setDead();
        $this->assertEquals(\GameOfLife\Cell::DEAD_STATE, $cell->isAlive());
    }

    public function testCellCoords()
    {
        $x = 10;
        $y = 5;

        $cell = new \GameOfLife\Cell(new \GameOfLife\Coord($x, $y));

        $this->assertEquals($x, $cell->getCoord()->getX());
        $this->assertEquals($y, $cell->getCoord()->getY());
    }
}