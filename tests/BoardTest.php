<?php


class BoardTest extends \PHPUnit\Framework\TestCase
{
    public function testInitEmptyBoard()
    {
        $board = new \GameOfLife\Board();

        $this->assertEquals(0, $board->getAliveCellsCount());
    }

    public function testGetAliveCellsCount()
    {
        $board = new \GameOfLife\Board();

        /** @var \GameOfLife\Cell $cell */
        $cell = $board[0][0];
        $cell->setAlive();

        $this->assertEquals(1, $board->getAliveCellsCount());
    }

    public function testGetAliveCellsCountAfterKill()
    {
        $board = new \GameOfLife\Board();

        /** @var \GameOfLife\Cell $cell */
        $cell = $board[0][0];
        $cell->setAlive();
        $cell->setDead();

        $this->assertEquals(0, $board->getAliveCellsCount());
    }

    public function testBoardSize()
    {
        $width = 10;
        $height = 20;

        $board = new \GameOfLife\Board($width, $height);

        $this->assertEquals($height, $board->getHeight());
        $this->assertEquals($width, $board->getWidth());
    }

    public function testGetCellAliveNeighboursCount()
    {
        $board = new \GameOfLife\Board();

        /** @var \GameOfLife\Cell $cell */
        $cell = $board[0][0];
        $cell->setAlive();

        $this->assertEquals(0, $board->getCellAliveNeighboursCount(0, 0));
        $this->assertEquals(1, $board->getCellAliveNeighboursCount(1, 0));
        $this->assertEquals(1, $board->getCellAliveNeighboursCount(1, 1));
        $this->assertEquals(1, $board->getCellAliveNeighboursCount(1, 0));
    }

    public function testGetCellOutOfBoundsAliveNeighboursCount()
    {
        $board = new \GameOfLife\Board();

        $this->assertEquals(0, $board->getCellAliveNeighboursCount(100, 100));
        $this->assertEquals(0, $board->getCellAliveNeighboursCount(-100, -100));
    }

    public function testGetCell()
    {
        $board = new \GameOfLife\Board();

        $cell = new \GameOfLife\Cell(new \GameOfLife\Coord(0, 0));
        $board[0][0] = $cell;

        $this->assertEquals($cell, $board->getCell(0, 0));
    }

    public function testGetCellOutOfBounds()
    {
        $board = new \GameOfLife\Board();

        $this->assertEquals(null, $board->getCell(100, 100));
    }

    public function testGenerateRandomAliveCells()
    {
        $aliveCellsCount = 10;
        $board = new \GameOfLife\Board(\GameOfLife\Board::DEFAULT_WIDTH, \GameOfLife\Board::DEFAULT_HEIGHT, $aliveCellsCount);

        $this->assertEquals($aliveCellsCount, $board->getAliveCellsCount());
    }

    public function testGenerateRandomAliveCellsOutOfBounds()
    {
        $boardSize = \GameOfLife\Board::DEFAULT_WIDTH * \GameOfLife\Board::DEFAULT_HEIGHT;
        $aliveCellsCount = 150;
        $board = new \GameOfLife\Board(\GameOfLife\Board::DEFAULT_WIDTH, \GameOfLife\Board::DEFAULT_HEIGHT, $aliveCellsCount);

        $this->assertEquals($boardSize, $board->getAliveCellsCount());
    }
}