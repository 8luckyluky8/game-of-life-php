<?php

class GameTest extends \PHPUnit\Framework\TestCase
{
    public function testGetCellNewStateDead()
    {
        $game = new \GameOfLife\Game();

        $this->assertEquals(\GameOfLife\Cell::DEAD_STATE, $game->getCellNewState(0, 0));
    }

    public function testGetCellNewStateAlive()
    {
        $game = new \GameOfLife\Game();
        $board = $game->getBoard();

        $cell = $board->getCell(0, 0);
        $cell->setAlive();

        $cell = $board->getCell(1, 0);
        $cell->setAlive();

        $cell = $board->getCell(2, 0);
        $cell->setAlive();

        $this->assertEquals(\GameOfLife\Cell::ALIVE_STATE, $game->getCellNewState(1, 0));
    }

    public function testGetCellNewStateOneStaysAliveAndOneBecomesAlive()
    {
        $game = new \GameOfLife\Game();
        $board = $game->getBoard();

        $cell = $board->getCell(0, 0);
        $cell->setAlive();

        $cell = $board->getCell(1, 0);
        $cell->setAlive();

        $cell = $board->getCell(2, 0);
        $cell->setAlive();

        $this->assertEquals(\GameOfLife\Cell::ALIVE_STATE, $game->getCellNewState(1, 0));
        $this->assertEquals(\GameOfLife\Cell::DEAD_STATE, $game->getCellNewState(0, 0));
        $this->assertEquals(\GameOfLife\Cell::DEAD_STATE, $game->getCellNewState(2, 0));
        $this->assertEquals(\GameOfLife\Cell::ALIVE_STATE, $game->getCellNewState(1, 1));
    }

    public function testGetNextStepAliveBoard()
    {
        $game = new \GameOfLife\Game();
        $board = $game->getBoard();

        $cell = $board->getCell(0, 0);
        $cell->setAlive();

        $cell = $board->getCell(1, 0);
        $cell->setAlive();

        $cell = $board->getCell(2, 0);
        $cell->setAlive();

        $nextStepBoard = $game->getNextStepBoard();
        $this->assertEquals(2, $nextStepBoard->getAliveCellsCount());
        $this->assertEquals(true, $nextStepBoard->getCell(1, 0)->isAlive());
        $this->assertEquals(false, $nextStepBoard->getCell(0, 0)->isAlive());
        $this->assertEquals(false, $nextStepBoard->getCell(2, 0)->isAlive());
        $this->assertEquals(true, $nextStepBoard->getCell(1, 1)->isAlive());
    }

    public function testGetNextStepDeadBoard()
    {
        $game = new \GameOfLife\Game();
        $board = $game->getBoard();

        $cell = $board->getCell(0, 0);
        $cell->setAlive();

        $cell = $board->getCell(1, 0);
        $cell->setAlive();

        $nextStepBoard = $game->getNextStepBoard();
        $this->assertEquals(0, $nextStepBoard->getAliveCellsCount());
        $this->assertEquals(false, $nextStepBoard->getCell(1, 0)->isAlive());
        $this->assertEquals(false, $nextStepBoard->getCell(0, 0)->isAlive());
    }
}