<?php

class CoordCollectionTest extends \PHPUnit\Framework\TestCase
{
    public function testIsCoordSet()
    {
        $coordCollection = new \GameOfLife\CoordCollection();

        $this->assertEquals(false, $coordCollection->isCoordSet(0, 0));

        $coord = new \GameOfLife\Coord(0, 0);
        $coordCollection[] = $coord;

        $this->assertEquals(true, $coordCollection->isCoordSet(0, 0));
    }
}